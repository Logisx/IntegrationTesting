# Color Code Converter API

This is a simple RESTful API for converting colors between RGB and Hexadecimal representations.

## Table of Contents

- [Color Code Converter API](#color-code-converter-api)
  - [Table of Contents](#table-of-contents)
  - [Features](#features)
  - [Prerequisites](#prerequisites)
  - [Getting Started](#getting-started)
    - [Installation](#installation)
      - [Query Parameters:](#query-parameters)
      - [Example:](#example)
    - [Hex to RGB Conversion](#hex-to-rgb-conversion)
      - [Query Parameters:](#query-parameters-1)
      - [Example:](#example-1)
  - [Testing](#testing)

## Features

- Convert RGB color values to Hexadecimal color codes.
- Convert Hexadecimal color codes to RGB color values.
- Error handling for invalid input.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js installed on your development machine.

## Getting Started

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/Logisx/IntegrationTesting.git
2. Install dependencies:

    ```bash
    . commands.sh
3. Running the Server
    ```bash
    npm start
- The API will be available at http://localhost:3001

## Usage
### RGB to Hex Conversion
To convert RGB color values to Hexadecimal color codes, make a GET request to the following endpoint:

```bash
http://localhost:3001/rgb-to-hex
```

#### Query Parameters:

- red: Red color value (0-255).
- green: Green color value (0-255).
- blue: Blue color value (0-255).
  
#### Example:

```bash
http://localhost:3001/rgb-to-hex?red=255&green=128&blue=0
```

### Hex to RGB Conversion
To convert Hexadecimal color codes to RGB color values, make a GET request to the following endpoint:

```bash
http://localhost:3001/hex-to-rgb
```
#### Query Parameters:
- hex: Hexadecimal color code (e.g., 'ff8000').
  
#### Example:
```bash
http://localhost:3001/hex-to-rgb?hex=ff8000
```
## Testing
You can test the API using a REST Client or Postman. Refer to the **'rest.http'** for detailed test cases and examples.
