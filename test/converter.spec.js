// test/converter.spec.js - Unit tests

const converter = require('../src/converter');
const { expect } = require('chai');


describe("Color Code Converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); //ff0000
            const greenHex = converter.rgbToHex(0, 255, 0); //00ff00
            const blueHex = converter.rgbToHex(0, 0, 255); //0000ff

            expect(redHex).to.equal('ff0000');
            expect(greenHex).to.equal('00ff00');
            expect(blueHex).to.equal('0000ff');
        });

        it("converts white to hex", () => {
            const whiteHex = converter.rgbToHex(255, 255, 255);
            expect(whiteHex).to.equal('ffffff');
        });
        
        it("converts black to hex", () => {
            const blackHex = converter.rgbToHex(0, 0, 0);
            expect(blackHex).to.equal('000000');
        });
        
        it("pads single character hex values", () => {
            // Test the pad function with single character hex values
            const singleCharHex1 = converter.pad('f'); // Should pad to '0f'
            const singleCharHex2 = converter.pad('7'); // Should pad to '07'

            expect(singleCharHex1).to.equal('0f');
            expect(singleCharHex2).to.equal('07');
        });
    });

    describe("Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            const red = converter.hexToRgb('ff0000');
            const green = converter.hexToRgb('00ff00');
            const blue = converter.hexToRgb('0000ff');

            expect(red).to.deep.equal({ red: 255, green: 0, blue: 0 });
            expect(green).to.deep.equal({ red: 0, green: 255, blue: 0 });
            expect(blue).to.deep.equal({ red: 0, green: 0, blue: 255 });
        });

        it("converts white hex to RGB", () => {
            const whiteRgb = converter.hexToRgb('ffffff');
            expect(whiteRgb).to.deep.equal({ red: 255, green: 255, blue: 255 });
        });
        
        it("converts black hex to RGB", () => {
            const blackRgb = converter.hexToRgb('000000');
            expect(blackRgb).to.deep.equal({ red: 0, green: 0, blue: 0 });
        });
    });
});



