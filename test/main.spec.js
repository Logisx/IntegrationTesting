// ./test/main.spec.js - Integration tests

const request = require('request');
const { expect } = require('chai');
const app = require('../src/main');
const port = 3001;

describe('Color Code Converter API', () => {
    let server;
    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: http://localhost:${port}`);
            done();
        });
    });    

    describe('RGB to Hex conversion', () => {
        const validUrl = `http://localhost:${port}/rgb-to-hex?red=255&green=128&blue=0`;
        const invalidUrl = `http://localhost:${port}/rgb-to-hex?red=300&green=128&blue=0`; // Invalid red value

        it('returns status 200 for valid RGB', (done) => {
            request(validUrl, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it('returns the color in hex for valid RGB', (done) => {
            request(validUrl, (error, response, body) => {
                expect(body).to.equal('ff8000');
                done();
            });
        });

        it('returns status 400 for invalid RGB', (done) => {
            request(invalidUrl, (error, response, body) => {
                expect(response.statusCode).to.equal(400);
                done();
            });
        });

        it('returns an error message for invalid RGB', (done) => {
            request(invalidUrl, (error, response, body) => {
                const parsedBody = JSON.parse(body);
                expect(parsedBody.error).to.equal('Invalid RGB values');
                done();
            });
        });
    });

    describe('Hex to RGB conversion', () => {
        const validUrl = `http://localhost:${port}/hex-to-rgb?hex=ff8000`;
        const invalidUrl = `http://localhost:${port}/hex-to-rgb?hex=ff8z00`; // Invalid hex format

        it('returns status 200 for valid hex', (done) => {
            request(validUrl, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it('returns the color in RGB for valid hex', (done) => {
            request(validUrl, (error, response, body) => {
                expect(body).to.equal('{"red":255,"green":128,"blue":0}');
                done();
            });
        });

        it('returns status 400 for invalid hex', (done) => {
            request(invalidUrl, (error, response, body) => {
                expect(response.statusCode).to.equal(400);
                done();
            });
        });

        it('returns an error message for invalid hex', (done) => {
            request(invalidUrl, (error, response, body) => {
                const parsedBody = JSON.parse(body);
                expect(parsedBody.error).to.equal('Invalid hex value');
                done();
            });
        });
    });

    after("Stop server after tests", (done) => {
        server.close();
        done();
    });
});
