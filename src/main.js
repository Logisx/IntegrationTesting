// src/main.js

const express = require('express');
const converter = require('./converter')
const app = express();
const port = 3001;

app.get('/', (req, res) => {
  res.send('Hello World!')
})

// endpoint localhost:3001/rgb-to-hex?red=255&green=128&blue=0
app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.red, 10);
    const green = parseInt(req.query.green, 10);
    const blue = parseInt(req.query.blue, 10);

    // Check if any of the parameters are missing or out of range
    if (isNaN(red) || isNaN(green) || isNaN(blue) || red < 0 || red > 255 || green < 0 || green > 255 || blue < 0 || blue > 255) {
      return res.status(400).json({ error: 'Invalid RGB values' });
    }
    
    const hex = converter.rgbToHex(red, green, blue);
    res.send(hex)
})

// endpoint localhost:3001/hex-to-rgb?hex=ff8000
app.get('/hex-to-rgb', (req, res) => {
    const hex = req.query.hex;

    // Check if the hex parameter is missing or has an invalid format
    if (!hex.match(/^([0-9A-Fa-f]{3}){1,2}$/)) {
      return res.status(400).json({ error: 'Invalid hex value' });
    }

    const rgb = converter.hexToRgb(hex);
    res.json(rgb);
});

if (process.env.NODE_ENV === 'test') {
  module.exports = app
} else {
  app.listen(port, () => {
    console.log(`Server:  http://localhost:${port}`)
  });
}

module.exports = app;