// src/converter.js

/**
 * Padding outputs 2 characters always
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}


/**
 * Convert an RGB color to a hex color code.
 * @param {number} red 0-255
 * @param {number} green 0-255
 * @param {number} blue 0-255
 * @returns {string} hex color code
 */
const rgbToHex = (red, green, blue) => {
    const redHex = red.toString(16); // may return single char
    const greenHex = green.toString(16);
    const blueHex = blue.toString(16);
    return pad(redHex) + pad(greenHex) + pad(blueHex);
}

/**
 * Convert a hex color code to an RGB object.
 * @param {string} hex - hex color code 
 * @returns {object} An object with red, green, and blue values.
*/
const hexToRgb = (hex) => {
    // Remove the hash symbol if it exists
    hex = hex.replace(/^#/, '');
    
    // Parse the hex values for red, green, and blue
    const red = parseInt(hex.substring(0, 2), 16);
    const green = parseInt(hex.substring(2, 4), 16);
    const blue = parseInt(hex.substring(4, 6), 16);
    
    // Return an object with the RGB values
    return { red, green, blue };
    
};

module.exports = {
    pad,
    rgbToHex,
    hexToRgb
};