#!/bin/bash

npm init -y
npm i -s express
npm i --save-dev mocha chai request

mkdir src
mkdir test

touch src/main.js
touch src/converter.js

touch test/main.spec.js
touch test/converter.spec.js

echo "node_modules" > .gitignore
